package lib.paradocs.run;

import java.io.*;

import javax.swing.*;

import lib.paradocs.io.*;
import lib.paradocs.gui.*;

public final class Main
{
	public static void main(String[] args)
	{
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch(Exception e) {
			e.printStackTrace();
			System.exit(1);
		}

		try {
			File dirUser = new File(System.getProperty("user.dir"));
			File dirResources = new File(dirUser,"/src/main/resources");
			File dirIcons = new File(dirResources,"icons");
			File fileConfig = new File(dirResources,"settings.cfg");
			GraphicalResources.load(dirIcons);
			ConfigParser.restoreDefaults();
			ConfigParser.load(fileConfig);
		}
		catch(IOException ioe) {
			ioe.printStackTrace();
		}
		finally {
			Master master = new Master();
			master.setVisible(true);
		}
	}
}