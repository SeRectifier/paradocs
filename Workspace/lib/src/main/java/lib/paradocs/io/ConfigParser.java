package lib.paradocs.io;

import java.io.*;
import java.util.*;

public final class ConfigParser
{
	private static final HashMap<String,Integer> mapInteger = new HashMap<String,Integer>(0);

	public static void restoreDefaults()
	{
		ConfigParser.mapInteger.put("default_window_width",960);
		ConfigParser.mapInteger.put("default_window_height",540);
		ConfigParser.mapInteger.put("default_filechooser_width",600);
		ConfigParser.mapInteger.put("default_filechooser_height",400);
		ConfigParser.mapInteger.put("default_texteditor_width",300);
		ConfigParser.mapInteger.put("default_texteditor_height",200);
	}

	public static void load(File file) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(file));

		int index = 0;
		String left,right;
		String cache = reader.readLine();
		while(cache != null) {
			index = cache.indexOf(":");
			if(index == -1 || index == cache.length()) continue;

			left = cache.substring(0,index);
			right = cache.substring(index + 1);
			ConfigParser.mapInteger.put(left,Integer.parseInt(right));

			cache = reader.readLine();
		}

		reader.close();
	}

	public static int getValue(String a)
	{
		return ConfigParser.mapInteger.get(a);
	}
}
