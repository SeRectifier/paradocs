package lib.paradocs.io;

import java.awt.image.*;
import java.io.*;
import java.util.*;
import javax.imageio.*;
import javax.swing.*;

public final class GraphicalResources
{
	private static final HashMap<String,ImageIcon> mapIcon = new HashMap<String,ImageIcon>(0);

	public static void load(File dir)
	{
		File[] arrayScan = dir.listFiles();

		String name = null;
		String key = null;
		BufferedImage img = null;
		for(File file : arrayScan) {
			if(file.isDirectory()) continue;
			if(file.getName().endsWith(".png") == false) continue;
			try {
				img = ImageIO.read(file);
				name = file.getName();
				key = name.substring(0,name.lastIndexOf("."));
				if(img.getWidth() != img.getHeight()) continue;
				GraphicalResources.mapIcon.put(key,new ImageIcon(img));
			}
			catch(IOException ioe) {
				ioe.printStackTrace();
			}
		}
	}

	public static ImageIcon getValue(String key)
	{
		return GraphicalResources.mapIcon.get(key);
	}
}
