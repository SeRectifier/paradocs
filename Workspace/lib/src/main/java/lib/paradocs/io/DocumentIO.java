package lib.paradocs.io;

import java.io.*;
import java.util.*;
import lib.paradocs.core.*;

public final class DocumentIO
{
	public static Document read(File file) throws IOException,ClassNotFoundException
	{
		ObjectInputStream stream = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)));
		String title = (String) stream.readObject();
		String author = (String) stream.readObject();
		Date date = (Date) stream.readObject();
		Section rs = (Section) stream.readObject();
		stream.close();

		Document doc = new Document(title,author,date,rs);
		return doc;
	}

	public static boolean write(File file,Document doc) throws IOException
	{
		if(file == null || doc == null) return false;
		if(file.isDirectory()) return false;

		ObjectOutputStream stream = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
		stream.writeObject(doc.getTitle());
		stream.writeObject(doc.getAuthor());
		stream.writeObject(doc.getLastModifiedDate());
		stream.writeObject(doc.getRootSection());
		stream.close();

		return true;
	}
}
