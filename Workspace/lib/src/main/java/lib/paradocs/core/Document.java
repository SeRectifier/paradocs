package lib.paradocs.core;

import java.util.*;
import javax.swing.event.*;
import javax.swing.tree.*;

/**
 * Documentは，文書を示すクラスです．文書要素を格納し，TreeModelインターフェースを実装することで木構造を扱えるようにします．
 * 
 * @author SeRectifier
 * @since alpha
 */
public class Document implements TreeModel
{
	public static final String DEFAULT_TITLE = "Untitled document";

	protected Vector<TreeModelListener> vectorListener = new Vector<TreeModelListener>(0);

	protected String textTitle = Document.DEFAULT_TITLE;
	protected String textAuthor = null;
	protected Date date = new Date();
	protected Section root = new Section(this.textTitle);

	/**
	 * デフォルトのタイトルで文書を初期化します．
	 */
	public Document()
	{
		this.setTitle(null);
	}

	/**
	 * タイトルを指定して文書を初期化します．
	 * @param t 文書のタイトル
	 */
	public Document(String t)
	{
		this.setTitle(t);
	}

	/**
	 * タイトルと著者名を指定して文書を初期化します．
	 * @param t 文書のタイトル
	 * @param a 文書の著者名
	 */
	public Document(String t,String a)
	{
		this.setTitle(t);
		this.setAuthor(a);
	}

	/**
	 * 
	 * @param t
	 * @param a
	 * @param d
	 * @param rs
	 */
	public Document(String t,String a,Date d,Section rs)
	{
		if(d == null) throw new IllegalArgumentException("Invalid date");
		if(rs == null) throw new IllegalArgumentException("Invalid root section");

		this.setTitle(t);
		this.setAuthor(a);
		this.date = d;
		this.root = rs;
	}

	/**
	 * removeElement(Element)によって呼び出され，再帰的処理を行います．
	 * @param p 探索対象の要素
	 * @param t 削除する要素
	 */
	protected void removeRecursiveScan(Element p,Element t)
	{
		int count = p.getChildCount();
		Element current = null;
		for(int i=0; i<count; i++) {
			current = p.getChildAt(i);
			if(current.equals(t)) {
				p.removeChildAt(p.indexOfChild(t));
				return;
			}
			else if(current.isParent()) this.removeRecursiveScan(current,t);
			else continue;
		}
	}

	/**
	 * 文書のタイトルを指定します．引数をnullにするとデフォルトのタイトルに設定されます．
	 * @param t　新しいタイトル
	 */
	public void setTitle(String t)
	{
		if(t == null) this.textTitle = Document.DEFAULT_TITLE;
		else if(t.trim().length() == 0) this.textTitle = Document.DEFAULT_TITLE;
		else {
			this.textTitle = t;
			this.root.setName(t);
		}
	}

	/**
	 * 文書の著者名を指定します．
	 * @param a 新しい著者名
	 */
	public void setAuthor(String a)
	{
		this.textAuthor = a;
	}

	/**
	 * 現在の文書のタイトルを返します．
	 * @return 文書のタイトル
	 */
	public String getTitle()
	{
		return this.textTitle;
	}

	/**
	 * 現在の文書の著者名を返します．
	 * @return 文書の著者名
	 */
	public String getAuthor()
	{
		return this.textAuthor;
	}

	/**
	 * 文書の更新日を現在の時刻に設定します．
	 */
	public void touch()
	{
		this.date = new Date();
	}

	/**
	 * 文書の更新日を返します．
	 * @return 文書の最終更新日
	 */
	public Date getLastModifiedDate()
	{
		return this.date;
	}

	/**
	 * 文書の根源セクションを返します．
	 * @return 根源セクション
	 */
	public Section getRootSection()
	{
		return this.root;
	}

	/**
	 * 文書要素を末尾に追加します．
	 * @param e 新しい文書要素
	 */
	public void addElement(Element e)
	{
		this.root.addChild(e);
	}

	/**
	 * index番目の文書要素を削除します．
	 * @param index 削除する文書要素のインデックス
	 */
	public void removeElementAt(int index)
	{
		this.root.removeChildAt(index);
	}

	/**
	 * 直系にある文書要素を削除します．子を持つ要素にネストされた要素は削除されません．
	 * @param e 削除する直系の文書要素
	 */
	public void removeLinealElement(Element e)
	{
		this.root.removeChild(e);
	}

	/**
	 * この文書が包含する要素を探索して，指定された要素を削除します．
	 * @param t 削除する文書要素
	 */
	public void removeElement(Element t)
	{
		this.removeRecursiveScan(this.root,t);
	}

	/**
	 * この文書に登録されているTreeModelListenerの全てのtreeStructureChanged()を呼び出し，文書が変更されたことを通知します．
	 */
	public void reflectChanges()
	{
		TreeModelEvent event = new TreeModelEvent(this.root,new TreePath(this.root));
		for(TreeModelListener listener : this.vectorListener) listener.treeStructureChanged(event);
	}

	/**
	 * 指定された文書要素が変更されたことを，リスナーのtreeNodesChangedを呼び出すことで通知します．
	 * @param src 変更を行ったことを通知したい文書要素
	 */
	public void reflectTerminalModification(Element src)
	{
		TreeModelEvent event = new TreeModelEvent(src,new TreePath(this.root));
		for(TreeModelListener listener : this.vectorListener) listener.treeNodesChanged(event);
	}

	public boolean matchesRootSection(Element s)
	{
		return s.equals(this.root);
	}

	@Override
	public String toString()
	{
		return this.textTitle;
	}

	@Override
	public Object getRoot()
	{
		return this.root;
	}

	@Override
	public Object getChild(Object parent, int index)
	{
		if(parent instanceof Element) {
			Element p = (Element) parent;
			return p.getChildAt(index);
		}
		else return null;
	}

	@Override
	public int getChildCount(Object parent)
	{
		if(parent instanceof Element) {
			Element p = (Element) parent;
			return p.getChildCount();
		}
		else return -1;
	}

	@Override
	public boolean isLeaf(Object node)
	{
		if(node instanceof Element) {
			Element n = (Element) node;
			return n.isTerminal();
		}
		else return false;
	}

	@Override
	public void valueForPathChanged(TreePath path,Object newValue) {}

	@Override
	public int getIndexOfChild(Object parent, Object child)
	{
		if(parent instanceof Element && child instanceof Element) {
			Element p = (Element) parent;
			Element c = (Element) child;
			return p.indexOfChild(c);
		}
		else return -1;
	}

	@Override
	public void addTreeModelListener(TreeModelListener l)
	{
		if(l != null) this.vectorListener.add(l);
	}

	@Override
	public void removeTreeModelListener(TreeModelListener l)
	{
		if(l != null) {
			this.vectorListener.remove(l);
			this.vectorListener.trimToSize();
		}
	}
}
