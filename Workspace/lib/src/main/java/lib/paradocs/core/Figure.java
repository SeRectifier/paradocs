package lib.paradocs.core;

import java.awt.image.*;

public class Figure extends Element
{
	private static final long serialVersionUID = 2179165243161312463L;

	public static final int DEFAULT_WIDTH = 400;
	public static final int DEFAULT_HEIGHT = 300;

	protected int width = 0;
	protected int height = 0;
	protected int[][] grid = null;

	public Figure()
	{
		this.width = Figure.DEFAULT_WIDTH;
		this.height = Figure.DEFAULT_HEIGHT;
		this.initialize();
	}

	public Figure(String n)
	{
		super(n);
		this.setSize(Figure.DEFAULT_WIDTH,Figure.DEFAULT_HEIGHT);
	}

	public Figure(String n,int w,int h)
	{
		super(n);
		this.setSize(w,h);
	}

	public Figure(BufferedImage img)
	{
		if(img == null) throw new IllegalArgumentException("Null source image");

		this.width = img.getWidth();
		this.height = img.getHeight();
		this.grid = new int[this.width][this.height];

		for(int x=0; x<this.width; x++) {
		for(int y=0; y<this.height; y++) {
			this.grid[x][y] = img.getRGB(x,y);
		}}
	}

	protected Figure(int w,int h,int[][] g)
	{
		this.width = w;
		this.height = h;
		this.grid = new int[this.width][this.height];

		for(int x=0; x<this.width; x++) {
		for(int y=0; y<this.height; y++) {
			this.grid[x][y] = g[x][y];
		}}
	}

	protected void initialize()
	{
		this.grid = new int[this.width][this.height];
		for(int x=0; x<this.width; x++) {
		for(int y=0; y<this.height; y++) {
			this.grid[x][y] = 0;
		}}
	}

	public void setColorAt(int x,int y,int c)
	{
		if(x < 0 || y < 0 || this.width <= x || this.height <= y) return;
		this.grid[x][y] = c;
	}

	public void setColorAtNoCheck(int x,int y,int c)
	{
		this.grid[x][y] = c;
	}

	public void setWidth(int w)
	{
		this.setSize(w,this.height);
	}

	public void setHeight(int h)
	{
		this.setSize(this.width,h);
	}

	public void setSize(int w,int h)
	{
		if(w < 1 || h < 1) throw new IllegalArgumentException("Invalid size");

		int widthOld = this.width;
		int heightOld = this.height;
		int[][] gridOld = this.grid;

		this.width = w;
		this.height = h;
		this.grid = new int[this.width][this.height];
		for(int x=0; x<this.width; x++) {
		for(int y=0; y<this.height; y++) {
			this.grid[x][y] = widthOld <= x || heightOld <= y ? 0 : gridOld[x][y];
		}}
	}

	public BufferedImage toBufferedImage()
	{
		BufferedImage image = new BufferedImage(this.width,this.height,BufferedImage.TYPE_INT_ARGB);

		for(int x=0; x<this.width; x++) {
		for(int y=0; y<this.height; y++) {
			image.setRGB(x,y,this.grid[x][y]);
		}}

		return image;
	}

	@Override
	public boolean isParent()
	{
		return false;
	}

	@Override
	public boolean isTerminal()
	{
		return true;
	}

	@Override
	public boolean contains(Element e)
	{
		return this.equals(e);
	}

	@Override
	public int getChildCount()
	{
		return 0;
	}

	@Override
	public int indexOfChild(Element e)
	{
		return -1;
	}

	@Override
	public Element getChildAt(int index)
	{
		return null;
	}

	@Override
	public Element removeChildAt(int index)
	{
		return null;
	}

	@Override
	public Element duplicate()
	{
		Figure c = new Figure(this.width,this.height,this.grid);
		this.duplicatePreliminary(c);
		return c;
	}
}
