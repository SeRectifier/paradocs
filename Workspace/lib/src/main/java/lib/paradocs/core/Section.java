package lib.paradocs.core;

import java.util.*;

public class Section extends Element
{
	private static final long serialVersionUID = -5922255086741207538L;

	protected ArrayList<Element> listChild = new ArrayList<Element>(0);

	public Section()
	{
		super();
	}

	public Section(String name)
	{
		super(name);
	}

	@Override
	public Element duplicate()
	{
		Section c = new Section();
		super.duplicatePreliminary(c);

		for(Element e : this.listChild) {
			if(e == null) continue;
			c.addChild(e);
		}

		return c;
	}

	public boolean containsLineal(Element e)
	{
		for(Element c : this.listChild) if(c.equals(e)) return true;
		return false;
	}

	public boolean addChild(Element e)
	{
		if(e == null) return false;
		this.listChild.add(e);
		return true;
	}

	public boolean removeChild(Element e)
	{
		if(e == null) return false;
		return this.listChild.remove(e);
	}

	public Element removeChildRegenerative(int index)
	{
		Element outsider = this.listChild.remove(index);
		this.listChild.trimToSize();
		return outsider;
	}

	public Element getElementAt(int index)
	{
		if(index < 0 || this.listChild.size() <= index) return null;
		return this.listChild.get(index);
	}

	@Override
	public boolean isParent()
	{
		return true;
	}

	@Override
	public boolean isTerminal()
	{
		return false;
	}

	@Override
	public boolean contains(Element e)
	{
		if(e == null) return false;

		for(Element current : this.listChild) {
			if(current.isParent()) return current.contains(e);
			else if(current.contains(e)) return true;
			else continue;
		}

		return false;
	}

	@Override
	public int getChildCount()
	{
		return this.listChild.size();
	}

	@Override
	public int indexOfChild(Element e)
	{
		return this.listChild.indexOf(e);
	}

	@Override
	public Element removeChildAt(int index)
	{
		if(index < 0 || this.listChild.size() <= index) return null;
		Element e = this.listChild.remove(index);
		this.listChild.trimToSize();
		return e;
	}

	@Override
	public Element getChildAt(int index)
	{
		if(index < 0 || this.listChild.size() <= index) return null;
		else return this.listChild.get(index);
	}
}
