package lib.paradocs.core;

import java.io.*;

public abstract class Element implements Serializable
{
	private static final long serialVersionUID = -597270420533918649L;

	public static final String DEFAULT_NAME = "Untitled element";
	public static final String DEFAULT_UNIQUE_STRING = "Undefined";

	protected String textName = null;
	protected String textUnique = null;
	protected String textDescription = null;

	protected Element()
	{
		super();
		this.setName(Element.DEFAULT_NAME);
		this.setUniqueString(Element.DEFAULT_UNIQUE_STRING);
	}

	protected Element(String n)
	{
		super();
		this.setName(n);
		this.setUniqueString(Element.DEFAULT_UNIQUE_STRING);
	}

	protected Element(String n,String u)
	{
		super();
		this.setName(n);
		this.setUniqueString(u);
	}

	protected void duplicatePreliminary(Element e)
	{
		e.setName(this.textName);
		e.setUniqueString(this.textUnique);
		e.setDescription(this.textDescription);
	}

	public abstract boolean isParent();
	public abstract boolean isTerminal();
	public abstract boolean contains(Element e);
	public abstract int getChildCount();
	public abstract int indexOfChild(Element e);
	public abstract Element getChildAt(int index);
	public abstract Element removeChildAt(int index);
	public abstract Element duplicate();

	public void setName(String n)
	{
		if(n == null) this.textName = Element.DEFAULT_NAME;
		else if(n.trim().length() == 0) this.textName = Element.DEFAULT_NAME;
		else this.textName = n;
	}

	public void setUniqueString(String us)
	{
		if(us == null) this.textUnique = Element.DEFAULT_UNIQUE_STRING;
		else if(us.trim().length() == 0) this.textUnique = Element.DEFAULT_UNIQUE_STRING;
		else this.textUnique = us;
	}

	public void setDescription(String d)
	{
		this.textDescription = d;
	}

	public String getName()
	{
		return this.textName;
	}

	public String getUniqueString()
	{
		return this.textUnique;
	}

	public String getDescription()
	{
		return this.textDescription;
	}

	@Override
	public String toString()
	{
		return this.textName;
	}
}
