package lib.paradocs.core;

public class Paragraph extends Element
{
	private static final long serialVersionUID = -4881608757740902546L;

	public Paragraph()
	{
		super();
	}

	public Paragraph(String n)
	{
		super(n);
	}

	public Paragraph(String n,String u)
	{
		super(n,u);
	}

	@Override
	public Element duplicate()
	{
		Paragraph c = new Paragraph();
		super.duplicatePreliminary(c);
		return c;
	}

	@Override
	public boolean isParent()
	{
		return false;
	}

	@Override
	public boolean isTerminal()
	{
		return true;
	}

	@Override
	public boolean contains(Element e)
	{
		return this.equals(e);
	}

	@Override
	public int getChildCount()
	{
		return 0;
	}

	@Override
	public int indexOfChild(Element e)
	{
		return -1;
	}

	@Override
	public Element removeChildAt(int index)
	{
		return null;
	}

	@Override
	public Element getChildAt(int index)
	{
		return null;
	}
}
