package lib.paradocs.core.format;

import java.io.*;

import lib.paradocs.core.*;

public abstract class AbstractFormatter implements Serializable
{
	private static final long serialVersionUID = -7299731578648196382L;

	public abstract void format(Document doc);
	public abstract void export(OutputStream stream);
}
