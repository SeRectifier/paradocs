package lib.paradocs.core;

import java.io.*;
import java.util.*;

public class Table extends Element
{
	private static final long serialVersionUID = -5743475012085878356L;

	protected ArrayList<Column> listColumn = new ArrayList<Column>(0);

	public Table()
	{
		super();
	}

	public Table(String n)
	{
		super(n);
	}

	public Table(String n,String u)
	{
		super(n,u);
	}

	public void addColumn(Column c)
	{
		if(c == null) return;
		this.listColumn.add(c);
	}

	public void setColumnAt(int index,Column c)
	{
		this.listColumn.set(index,c);
	}

	public Column getColumnAt(int index)
	{
		return this.listColumn.get(index);
	}

	public Column removeColumnAt(int index)
	{
		return this.listColumn.remove(index);
	}

	public void setColumnNameAt(int index,String n)
	{
		this.listColumn.get(index).setName(n);
	}

	public String getColumnNameAt(int index)
	{
		return this.listColumn.get(index).getName();
	}

	public void setValue(int it,int ic,String v)
	{
		this.listColumn.get(it).setValueAt(ic,v);
	}

	public String getValue(int it,int ic)
	{
		return this.listColumn.get(it).getValueAt(ic);
	}

	@Override
	public boolean isParent()
	{
		return false;
	}

	@Override
	public boolean isTerminal()
	{
		return true;
	}

	@Override
	public boolean contains(Element e)
	{
		return false;
	}

	@Override
	public int getChildCount()
	{
		return 0;
	}

	@Override
	public int indexOfChild(Element e)
	{
		return -1;
	}

	@Override
	public Element getChildAt(int index)
	{
		return null;
	}

	@Override
	public Element removeChildAt(int index)
	{
		return null;
	}

	@Override
	public Element duplicate()
	{
		Table c = new Table(this.textName);
		super.duplicatePreliminary(c);

		for(Column column : this.listColumn) c.addColumn(column.clone());

		return c;
	}

	public class Column implements Serializable,Cloneable
	{
		private static final long serialVersionUID = -866976435048115076L;

		public static final String DEFAULT_NAME = "Unnamed column";

		protected String textName = Column.DEFAULT_NAME;
		protected ArrayList<String> values = new ArrayList<String>(0);

		public Column() {}

		@Override
		public Column clone()
		{
			Column c = new Column();

			c.setName(this.textName);
			for(int i=0; i<this.values.size(); i++) c.setValueAt(i,this.values.get(i));

			return c;
		}

		public void setName(String n)
		{
			if(n == null) this.textName = Column.DEFAULT_NAME;
			else if(n.trim().length() == 0) this.textName = Column.DEFAULT_NAME;
			else this.textName = n;
		}

		public String getName()
		{
			return this.textName;
		}

		public void set(String[] array)
		{
			if(array == null) return;
			this.values.clear();
			this.values.trimToSize();
			for(int i=0; i<array.length; i++) this.values.set(i,array[i]);
		}

		public void addValue(String v)
		{
			this.values.add(v);
		}

		public void setValueAt(int index,String v)
		{
			this.values.set(index,v);
		}

		public String getValueAt(int index)
		{
			return this.values.get(index);
		}

		public String removeValueAt(int index)
		{
			return this.values.remove(index);
		}

		public int getSize()
		{
			return this.values.size();
		}

		public void clear()
		{
			this.values.clear();
		}
	}
}
