package lib.paradocs.gui.event;

import java.awt.*;
import lib.paradocs.gui.tabs.AbstractElementTab;

public class TabCloseEvent extends AWTEvent
{
	private static final long serialVersionUID = -2959029789147438101L;

	protected AbstractElementTab tab = null;

	public TabCloseEvent(AbstractElementTab source, int id)
	{
		super(source,id);
		this.tab = source;
	}

	public AbstractElementTab getTab()
	{
		return this.tab;
	}
}
