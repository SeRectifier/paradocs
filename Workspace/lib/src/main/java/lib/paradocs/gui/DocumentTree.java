package lib.paradocs.gui;

import javax.swing.*;
import javax.swing.tree.*;

import lib.paradocs.core.*;

public class DocumentTree extends JTree
{
	private static final long serialVersionUID = -1162090007361126122L;

	protected Document document = null;

	public DocumentTree(Document doc)
	{
		super(doc);

		if(doc == null) throw new IllegalArgumentException("Null document");
		this.document = doc;
	}

	public Element getSelectedElement()
	{
		TreePath path = super.getSelectionPath();
		if(path == null) return null;
		Element element = (Element) path.getLastPathComponent();
		return element;
	}

	public void setDocument(Document doc)
	{
		this.document = doc;
		super.setModel(doc);
	}

	public Document getDocument()
	{
		return this.document;
	}
}
