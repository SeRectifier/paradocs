package lib.paradocs.gui.event;

import java.util.*;

public interface ElementUpdateListener extends EventListener
{
	public void elementUpdated(ElementUpdateEvent e);
}
