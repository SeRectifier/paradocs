package lib.paradocs.gui.event;

import java.awt.*;
import lib.paradocs.core.*;

public class ElementUpdateEvent extends AWTEvent
{
	private static final long serialVersionUID = 7740529703036049942L;

	protected Element element = null;

	public ElementUpdateEvent(Element source, int id)
	{
		super(source,id);
		this.element = source;
	}

	public Element getElement()
	{
		return this.element;
	}
}
