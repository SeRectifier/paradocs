package lib.paradocs.gui;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.util.*;
import javax.swing.*;
import lib.paradocs.core.*;

public class NewElementDialog extends JDialog
{
	private static final long serialVersionUID = 6438555901014077841L;

	private final JList<ElementLineUpEntry> list = new JList<ElementLineUpEntry>();
	private final JLabel labelDescription = new JLabel();
	private final SampleDisplay display = new SampleDisplay();
	private final JButton buttonDone = new JButton("Add");
	private final JButton buttonCancel = new JButton("Cancel");

	private Element response = null;

	public NewElementDialog(JFrame owner)
	{
		super(owner,"New element",true);

		this.initField();
		this.initListener();
		this.initPane();

		// TODO Make this process refer ConfigParser
		super.setSize(new Dimension(400,300));
		super.setResizable(true);
	}

	private void initField()
	{
		ElementLineUpEntry entry = null;
		ArrayList<ElementLineUpEntry> listELUE = new ArrayList<ElementLineUpEntry>(0);

		entry = new ElementLineUpEntry("Paragraph") {
			@Override
			public Element create()
			{
				return new Paragraph();
			}
		};
		listELUE.add(entry);

		entry = new ElementLineUpEntry("Figure") {
			@Override
			public Element create() {
				return new Figure();
			}
		};
		listELUE.add(entry);

		DefaultListModel<ElementLineUpEntry> model = new DefaultListModel<ElementLineUpEntry>();
		for(int i=0; i<listELUE.size(); i++) model.add(i,listELUE.get(i));

		this.list.setModel(model);
	}

	private void initListener()
	{
		DisposeListener listenerQuit = new DisposeListener();

		this.buttonCancel.addActionListener(listenerQuit);
		this.buttonDone.addActionListener(listenerQuit);
		super.addWindowListener(listenerQuit);
	}

	private void initPane()
	{
		JPanel paneCenter = new JPanel(new BorderLayout());
		paneCenter.add(this.labelDescription,BorderLayout.NORTH);
		paneCenter.add(this.display,BorderLayout.CENTER);

		JPanel paneSouth = new JPanel(new FlowLayout());
		paneSouth.add(this.buttonCancel);
		paneSouth.add(this.buttonDone);

		super.add(this.list,BorderLayout.WEST);
		super.add(paneCenter,BorderLayout.CENTER);
		super.add(paneSouth,BorderLayout.SOUTH);
	}

	public Element getResponse()
	{
		return this.response;
	}

	public abstract class ElementLineUpEntry
	{
		public static final String DEFAULT_TITLE = "Untitled element entry";

		protected String textTitle = ElementLineUpEntry.DEFAULT_TITLE;
		protected String textDescription = null;
		protected BufferedImage imageSample = null;

		protected ElementLineUpEntry(String t)
		{
			this.setTitle(t);
		}

		public void setTitle(String t)
		{
			if(t == null) this.textTitle = ElementLineUpEntry.DEFAULT_TITLE;
			else if(t.trim().length() == 0) this.textTitle = ElementLineUpEntry.DEFAULT_TITLE;
			else this.textTitle = t;
		}

		public void setDescription(String d)
		{
			this.textDescription = d;
		}

		public void setSampleImage(BufferedImage img)
		{
			this.imageSample = img;
		}

		public String getTitle()
		{
			return this.textTitle;
		}

		public String getDescription()
		{
			return this.textDescription;
		}

		public BufferedImage getImage()
		{
			return this.imageSample;
		}

		@Override
		public String toString()
		{
			return this.textTitle;
		}

		public abstract Element create();
	}

	final class SampleDisplay extends Canvas
	{
		private static final long serialVersionUID = 3528571236632492642L;

		private final int marginMin = 16;

		private int widthCanvas = 0;
		private int heightCanvas = 0;
		private int marginX = 0;
		private int marginY = 0;
		private int widthRender = 0;
		private int heightRender = 0;
		private float ratioCanvas = 0.0f;
		private float ratioImage = 0.0f;
		private BufferedImage image = null;

		public void setImage(BufferedImage img)
		{
			if(img == null) {
				this.image = null;
				this.ratioImage = 0.0f;
			}
			else {
				this.ratioImage = (float) img.getHeight() / (float) img.getWidth();
				this.image = img;
			}
		}

		@Override
		public void paint(Graphics g)
		{
			if(this.image == null) return;

			this.widthCanvas = super.getWidth();
			this.heightCanvas = super.getHeight();
			this.ratioCanvas = (float) this.heightCanvas / (float) this.widthCanvas;

			if(this.ratioImage < this.ratioCanvas) {
				this.marginX = this.marginMin;
				this.widthRender = this.widthCanvas - this.marginX * 2;
				this.heightRender = Math.round(this.widthRender * this.ratioImage);
				this.marginY = Math.round((this.heightCanvas - this.heightRender) / 2.0f);
			}
			else {
				this.marginY = this.marginMin;
				this.heightRender = this.heightCanvas - this.marginY * 2;
				this.widthRender = Math.round(this.heightRender / this.ratioImage);
				this.marginX = Math.round((this.widthCanvas - this.widthRender) / 2.0f);
			}

			g.drawImage(this.image,this.marginX,this.marginY,this.widthRender,this.heightRender,this);
		}
	}

	class DisposeListener extends WindowAdapter implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			Object src = e.getSource();

			if(src.equals(NewElementDialog.this.buttonCancel)) {
				NewElementDialog.this.response = null;
				NewElementDialog.super.dispose();
			}
			if(src.equals(NewElementDialog.this.buttonDone)) {
				ElementLineUpEntry entry = NewElementDialog.this.list.getSelectedValue();
				if(entry == null) return;
				NewElementDialog.this.response = entry.create();
				NewElementDialog.super.dispose();
			}
		}

		@Override
		public void windowClosing(WindowEvent e)
		{
			NewElementDialog.this.response = null;
			NewElementDialog.super.dispose();
		}
	}
}
