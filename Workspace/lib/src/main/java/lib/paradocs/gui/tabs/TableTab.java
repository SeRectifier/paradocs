package lib.paradocs.gui.tabs;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import lib.paradocs.core.*;
import lib.paradocs.gui.*;

public class TableTab extends AbstractElementTab
{
	private static final long serialVersionUID = 3260774462310215342L;

	private final JLabel labelName = new JLabel("Name:");
	private final JLabel labelUnique = new JLabel("Unique text:");
	private final JLabel labelDescription = new JLabel("Description:");
	private final JTextField fieldName = new JTextField();
	private final JTextField fieldUnique = new JTextField();
	private final JButton buttonDescription = new JButton("Edit...");
	private final JTable table = new JTable();

	protected TableTab(Table t)
	{
		super(t);

		this.initTable(t);

		this.initField();
		this.initListener();
		this.initPane();
	}

	private void initTable(Table t)
	{
		
	}

	private void initField()
	{
		this.fieldName.setText(super.target.getName());
		this.fieldUnique.setText(super.target.getUniqueString());
	}

	private void initListener()
	{
		ButtonListener listenerButton = new ButtonListener();
		FocusLostReflectionListener listenerFocusLostReflection = new FocusLostReflectionListener();
		this.buttonDescription.addActionListener(listenerButton);
		this.fieldName.addFocusListener(listenerFocusLostReflection);
		this.fieldUnique.addFocusListener(listenerFocusLostReflection);
	}

	private void initPane()
	{
		GridBagConstraints gbc = new GridBagConstraints();
		GridBagLayout layoutNorth = new GridBagLayout();
		JPanel paneNorth = new JPanel(layoutNorth);

		Component[] arrayLeft = new Component[] {
				this.labelName,this.labelUnique,this.labelDescription,
		};

		Component[] arrayRight = new Component[] {
				this.fieldName,this.fieldUnique,this.buttonDescription,
		};

		gbc.gridwidth = gbc.gridheight = 1;
		gbc.fill = GridBagConstraints.BOTH;

		gbc.gridx = 0;
		gbc.weightx = 0.0d;
		for(int i=0; i<arrayLeft.length; i++) {
			gbc.gridy = i;
			layoutNorth.setConstraints(arrayLeft[i],gbc);
			paneNorth.add(arrayLeft[i]);
		}

		gbc.gridx = 1;
		gbc.weightx = 1.0d;
		for(int i=0; i<arrayRight.length; i++) {
			gbc.gridy = i;
			layoutNorth.setConstraints(arrayRight[i],gbc);
			paneNorth.add(arrayRight[i]);
		}

		super.setLayout(new BorderLayout());
		super.add(paneNorth,BorderLayout.NORTH);
		super.add(this.table,BorderLayout.CENTER);
	}

	@Override
	public void processOnClose()
	{
		// TODO Auto-generated method stub

	}

	class ButtonListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			Object src = e.getSource();

			if(src.equals(TableTab.this.buttonDescription)) {
				Element target = TableTab.super.getTarget();
				String title = "Edit description";
				TinyTextEditor editor = new TinyTextEditor(title,true,target.getDescription());
				editor.setVisible(true);
				target.setDescription(editor.getModifiedText());
			}
		}
	}

	class FocusLostReflectionListener implements FocusListener
	{
		@Override
		public void focusGained(FocusEvent e) {}

		@Override
		public void focusLost(FocusEvent e)
		{
			TableTab.super.target.setName(TableTab.this.fieldName.getText());
			TableTab.super.target.setUniqueString(TableTab.this.fieldUnique.getText());
			TableTab.super.fireElementUpdateEvent();
		}
	}
}
