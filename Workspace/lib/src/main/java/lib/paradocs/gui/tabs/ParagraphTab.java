package lib.paradocs.gui.tabs;

import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.*;

import lib.paradocs.core.*;

public class ParagraphTab extends AbstractElementTab
{
	private static final long serialVersionUID = -8851948418440627532L;

	private final JLabel labelName = new JLabel("Name:");
	private final JLabel labelUnique = new JLabel("Unique text:");
	private final JLabel labelContent = new JLabel("Content:");
	private final JScrollPane scrollAreaWrapper = new JScrollPane();
	private final JTextField fieldName = new JTextField();
	private final JTextField fieldUnique = new JTextField();
	private final JTextArea area = new JTextArea();

	public ParagraphTab(Paragraph p)
	{
		super(p);

		this.initField();
		this.initListener();
		this.initPane();
	}

	private void initField()
	{
		this.area.setLineWrap(true);
		this.scrollAreaWrapper.getViewport().setView(this.area);

		this.fieldName.setText(super.target.getName());
		this.fieldUnique.setText(super.target.getUniqueString());
		this.area.setText(super.target.getDescription());
	}

	private void initListener()
	{
		FocusLostReflectionListener listenerFocusLostReflection = new FocusLostReflectionListener();
		this.fieldName.addFocusListener(listenerFocusLostReflection);
		this.fieldUnique.addFocusListener(listenerFocusLostReflection);
		this.area.addFocusListener(listenerFocusLostReflection);
	}

	private void initPane()
	{
		GridBagConstraints gbc = new GridBagConstraints();
		GridBagLayout layoutCenter = new GridBagLayout();
		JPanel paneCenter = new JPanel(layoutCenter);

		Component[] arrayLeft = new Component[] {
				this.labelName,this.labelUnique,
		};

		Component[] arrayRight = new Component[] {
				this.fieldName,this.fieldUnique,
		};

		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = 0.0d;
		gbc.weighty = 0.0d;
		gbc.gridx = 0;
		for(int i=0; i<arrayLeft.length; i++) {
			gbc.gridy = i;
			layoutCenter.setConstraints(arrayLeft[i],gbc);
			paneCenter.add(arrayLeft[i]);
		}

		gbc.weighty = 1.0d;
		gbc.gridy = arrayLeft.length;
		layoutCenter.setConstraints(this.labelContent,gbc);
		paneCenter.add(this.labelContent);

		gbc.weightx = 1.0d;
		gbc.weighty = 0.0d;
		gbc.gridx = 1;
		for(int i=0; i<arrayRight.length; i++) {
			gbc.gridy = i;
			layoutCenter.setConstraints(arrayRight[i],gbc);
			paneCenter.add(arrayRight[i]);
		}

		gbc.weighty = 1.0d;
		gbc.gridy = arrayRight.length;
		layoutCenter.setConstraints(this.scrollAreaWrapper,gbc);
		paneCenter.add(this.scrollAreaWrapper);

		super.setLayout(new BorderLayout());
		super.add(paneCenter,BorderLayout.CENTER);
	}

	@Override
	public void processOnClose()
	{
		super.target.setName(this.fieldName.getText());
		super.target.setUniqueString(this.fieldUnique.getText());
		super.target.setDescription(this.area.getText());
	}

	class FocusLostReflectionListener implements FocusListener
	{
		@Override
		public void focusGained(FocusEvent e) {}

		@Override
		public void focusLost(FocusEvent e)
		{
			ParagraphTab.super.target.setName(ParagraphTab.this.fieldName.getText());
			ParagraphTab.super.target.setUniqueString(ParagraphTab.this.fieldUnique.getText());
			ParagraphTab.super.target.setDescription(ParagraphTab.this.area.getText());
			ParagraphTab.super.fireElementUpdateEvent();
		}
	}
}
