package lib.paradocs.gui.tabs;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;
import lib.paradocs.core.*;
import lib.paradocs.gui.*;

public class FigureTab extends AbstractElementTab
{
	private static final long serialVersionUID = 5314779003348256902L;

	private final JLabel labelName = new JLabel("Name:");
	private final JLabel labelUnique = new JLabel("Unique text:");
	private final JLabel labelDescription = new JLabel("Description:");
	private final JLabel labelImage = new JLabel("Image:");
	private final JTextField fieldName = new JTextField();
	private final JTextField fieldUnique = new JTextField();
	private final JButton buttonDescription = new JButton("Edit...");
	private final JButton buttonImage = new JButton("Edit...");
	private final FigureCanvas canvas = new FigureCanvas();

	private BufferedImage img = null;

	public FigureTab(Figure f)
	{
		super(f);
		this.img = f.toBufferedImage();

		this.initField();
		this.initListener();
		this.initPane();
	}

	private void initField()
	{
		this.fieldName.setText(super.target.getName());
		this.fieldUnique.setText(super.target.getUniqueString());
	}

	private void initListener()
	{
		ButtonListener listenerButton = new ButtonListener();
		FocusLostReflectionListener listenerFocusLostReflection = new FocusLostReflectionListener();
		this.buttonDescription.addActionListener(listenerButton);
		this.buttonImage.addActionListener(listenerButton);
		this.fieldName.addFocusListener(listenerFocusLostReflection);
		this.fieldUnique.addFocusListener(listenerFocusLostReflection);
	}

	private void initPane()
	{
		GridBagConstraints gbc = new GridBagConstraints();
		GridBagLayout layoutNorth = new GridBagLayout();
		JPanel paneNorth = new JPanel(layoutNorth);

		Component[] arrayLeft = new Component[] {
				this.labelName,this.labelUnique,this.labelDescription,this.labelImage,
		};

		Component[] arrayRight = new Component[] {
				this.fieldName,this.fieldUnique,this.buttonDescription,this.buttonImage,
		};

		gbc.gridwidth = gbc.gridheight = 1;
		gbc.fill = GridBagConstraints.BOTH;

		gbc.gridx = 0;
		gbc.weightx = 0.0d;
		for(int i=0; i<arrayLeft.length; i++) {
			gbc.gridy = i;
			layoutNorth.setConstraints(arrayLeft[i],gbc);
			paneNorth.add(arrayLeft[i]);
		}

		gbc.gridx = 1;
		gbc.weightx = 1.0d;
		for(int i=0; i<arrayRight.length; i++) {
			gbc.gridy = i;
			layoutNorth.setConstraints(arrayRight[i],gbc);
			paneNorth.add(arrayRight[i]);
		}

		super.setLayout(new BorderLayout());
		super.add(paneNorth,BorderLayout.NORTH);
		super.add(this.canvas,BorderLayout.CENTER);
	}

	@Override
	public void processOnClose()
	{
		super.target.setName(this.fieldName.getText());
		super.target.setUniqueString(this.fieldUnique.getText());
	}

	class ButtonListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			Object src = e.getSource();

			if(src.equals(FigureTab.this.buttonDescription)) {
				Element target = FigureTab.super.getTarget();
				String title = "Edit description";
				TinyTextEditor editor = new TinyTextEditor(title,true,target.getDescription());
				editor.setVisible(true);
				target.setDescription(editor.getModifiedText());
			}

			if(src.equals(FigureTab.this.buttonImage)) {
				// TODO
			}
		}
	}

	class FocusLostReflectionListener implements FocusListener
	{
		@Override
		public void focusGained(FocusEvent e) {}

		@Override
		public void focusLost(FocusEvent e)
		{
			FigureTab.super.target.setName(FigureTab.this.fieldName.getText());
			FigureTab.super.target.setUniqueString(FigureTab.this.fieldUnique.getText());
			FigureTab.super.fireElementUpdateEvent();
		}
	}

	final class FigureCanvas extends Canvas
	{
		private static final long serialVersionUID = -7493250638722291930L;

		private int marginMin = 16;
		private int marginX = 0;
		private int marginY = 0;
		private int widthCanvas = 0;
		private int heightCanvas = 0;
		private int widthRender = 0;
		private int heightRender = 0;
		private float ratioCanvas = 0.0f;
		private float ratioImage = 0.0f;

		@Override
		public void paint(Graphics g)
		{
			if(FigureTab.this.img == null) return;

			this.widthCanvas = super.getWidth();
			this.heightCanvas = super.getHeight();
			this.ratioCanvas = (float) this.heightCanvas / (float) this.widthCanvas;
			this.ratioImage = (float) FigureTab.this.img.getHeight() / (float) FigureTab.this.img.getWidth();

			if(this.ratioImage < this.ratioCanvas) {
				this.marginX = this.marginMin;
				this.widthRender = this.widthCanvas - this.marginX * 2;
				this.heightRender = Math.round(this.widthRender * this.ratioImage);
				this.marginY = (this.heightCanvas - this.heightRender) / 2;
			}
			else {
				this.marginY = this.marginMin;
				this.heightRender = this.heightCanvas - this.marginY * 2;
				this.widthRender = Math.round(this.heightRender / this.ratioImage);
				this.marginX = (this.widthCanvas - this.widthRender) / 2;
			}

			g.clearRect(0,0,this.widthCanvas,this.heightCanvas);
			g.drawImage(FigureTab.this.img,this.marginX,this.marginY,this.widthRender,this.heightRender,this);
			g.drawRect(this.marginX,this.marginY,this.widthRender,this.heightRender);
		}
	}
}
