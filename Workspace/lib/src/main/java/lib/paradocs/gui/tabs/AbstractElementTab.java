package lib.paradocs.gui.tabs;

import java.util.Vector;
import javax.swing.*;
import lib.paradocs.core.*;
import lib.paradocs.gui.event.*;

/**
 * タブペインに追加される，文書要素を編集するためのタブの抽象クラスです．
 * @author SeRectifier
 * @since alpha
 */
public abstract class AbstractElementTab extends JPanel
{
	private static final long serialVersionUID = -1453452594789298949L;

	protected final Vector<ElementUpdateListener> vectorElementUpdateListener = new Vector<ElementUpdateListener>(0);

	protected Element target = null;

	/**
	 * サブクラスによって呼ばれるコンストラクタです．
	 * 編集対象としてElementを引数に取りますが，nullは許容されません．
	 * @param e 編集対象の文書要素
	 */
	protected AbstractElementTab(Element e)
	{
		if(e == null) throw new IllegalArgumentException("Null target element");
		this.target = e;
	}

	/**
	 * この編集タブに登録されているリスナーのdocumentUpdated()を呼び出します．
	 * 文書要素が何らかの編集を受けたことをリスナーに通知するために，サブクラスが呼び出すのに有用です．
	 */
	protected void fireElementUpdateEvent()
	{
		ElementUpdateEvent event = new ElementUpdateEvent(this.target,0);
		for(ElementUpdateListener listener : this.vectorElementUpdateListener) listener.elementUpdated(event);;
	}

	/**
	 * ElementUpdateListenerを追加します．
	 * @param l 新たに追加するElementUpdateListener
	 */
	public void addElementUpdateListener(ElementUpdateListener l)
	{
		if(l == null) return;
		this.vectorElementUpdateListener.add(l);
	}

	/**
	 * ElementUpdateListenerを削除します．
	 * @param l 削除するElementUpdateListener
	 */
	public void removeElementUpdateListener(ElementUpdateListener l)
	{
		if(l == null) return;
		this.vectorElementUpdateListener.remove(l);
	}

	/**
	 * 編集対象の文書要素を返します．
	 * @return 文書要素
	 */
	public Element getTarget()
	{
		return this.target;
	}

	/**
	 * 編集対象の文書要素の名前を返します．
	 * @return 文書要素の名前
	 */
	public String getElementName()
	{
		return this.target.getName();
	}

	/**
	 * この編集タブが閉じられるときに行うべき処理を記述します．
	 */
	public abstract void processOnClose();
}
