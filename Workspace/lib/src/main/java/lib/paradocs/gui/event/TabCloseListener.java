package lib.paradocs.gui.event;

import java.util.*;

public interface TabCloseListener extends EventListener
{
	public void tabClosed(TabCloseEvent e);
}
