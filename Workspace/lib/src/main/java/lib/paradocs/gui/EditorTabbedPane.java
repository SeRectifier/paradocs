package lib.paradocs.gui;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import lib.paradocs.core.*;
import lib.paradocs.gui.event.*;
import lib.paradocs.gui.tabs.AbstractElementTab;

public class EditorTabbedPane extends JTabbedPane
{
	private static final long serialVersionUID = 2734788213376649346L;

	protected final Vector<TabCloseListener> vectorTabCloseListener = new Vector<TabCloseListener>(0);

	/**
	 * 新しく要素編集タブを追加します．
	 * 但し，既に存在しているタブの編集対象と新しいタブの編集対象を照合します．
	 * 新しいタブの編集対象が既に他のタブによって開かれている場合には，新しくタブを開く代わりに同じ編集対象のタブをフォーカスします．
	 * そうでない場合には既に開かれているタブの末尾に新しくタブを追加します．
	 * @param tab 新しく追加するタブ
	 */
	public void openElementTab(AbstractElementTab tab)
	{
		if(tab == null) return;

		int count = super.getTabCount();

		Element elem = null;
		for(int i=0; i<count; i++) {
			elem = this.getElementTabAt(i).getTarget();
			if(elem.equals(tab.getTarget())) {
				super.setSelectedIndex(i);
				return;
			}
		}

		tab.addElementUpdateListener(new TabTitleCorrectionListener());
		super.addTab(tab.getElementName(),tab);
		int index = super.indexOfComponent(tab);
		super.setTabComponentAt(index,new TabHead(tab.getElementName()));
		super.setSelectedIndex(index);
	}

	public void closeElementTabAt(int index)
	{
		AbstractElementTab tab = this.getElementTabAt(index);
		this.closeElementTab(tab);
	}

	public void closeElementTab(Element e)
	{
		if(e == null) return;

		int count = super.getTabCount();
		ArrayList<AbstractElementTab> listTabToClose = new ArrayList<AbstractElementTab>(0);
		AbstractElementTab tab = null;
		for(int i=0; i<count; i++) {
			tab = this.getElementTabAt(i);
			if(e.contains(tab.getTarget())) {
				tab.processOnClose();
				TabCloseEvent event = new TabCloseEvent(tab,0);
				for(TabCloseListener listener : this.vectorTabCloseListener) listener.tabClosed(event);
				listTabToClose.add(tab);
			}
		}

		for(AbstractElementTab tabToClose : listTabToClose) super.removeTabAt(super.indexOfComponent(tabToClose));
	}

	public void closeElementTab(AbstractElementTab tab)
	{
		if(tab == null) return;

		tab.processOnClose();
		TabCloseEvent event = new TabCloseEvent(tab,0);
		for(TabCloseListener listener : this.vectorTabCloseListener) listener.tabClosed(event);
		super.removeTabAt(super.indexOfComponent(tab));
	}

	public void closeAllElementTabs()
	{
		int countElementTab = super.getTabCount();
		ArrayList<AbstractElementTab> listTabToClose = new ArrayList<AbstractElementTab>(countElementTab);
		for(int i=0; i<countElementTab; i++) listTabToClose.add(this.getElementTabAt(i));
		for(AbstractElementTab tab : listTabToClose) this.closeElementTab(tab);
	}

	public AbstractElementTab getSelectedElementTab()
	{
		return (AbstractElementTab) super.getSelectedComponent();
	}

	public AbstractElementTab getElementTabAt(int index)
	{
		if(index < 0 || super.getTabCount() <= index) return null;
		return (AbstractElementTab) super.getComponentAt(index);
	}

	public void addTabCloseListener(TabCloseListener l)
	{
		if(l == null) return;
		this.vectorTabCloseListener.add(l);
	}

	public void removeTabCloseListener(TabCloseListener l)
	{
		if(l == null) return;
		this.vectorTabCloseListener.remove(l);
	}

	class TabTitleCorrectionListener implements ElementUpdateListener
	{
		@Override
		public void elementUpdated(ElementUpdateEvent e)
		{
			Element elementSource = e.getElement();
			Element elementOpened = null;
			TabHead tabhead = null;
			int countOpenedTab = EditorTabbedPane.super.getTabCount();
			for(int i=0; i<countOpenedTab; i++) {
				elementOpened = EditorTabbedPane.this.getElementTabAt(i).getTarget();
				if(elementOpened.equals(elementSource)) {
					tabhead = (TabHead) EditorTabbedPane.super.getTabComponentAt(i);
					tabhead.setDisplayingTitle(elementSource.getName());
				}
			}
		}
	}

	class TabHead extends JPanel
	{
		static final long serialVersionUID = 3652098626148837662L;

		private final JLabel label = new JLabel();
		private final CloseCross button = new CloseCross();

		public TabHead(String title)
		{
			super(new BorderLayout());

			this.label.setText(title);

			this.initField();
			this.initListener();
			this.initPane();
		}

		public void setDisplayingTitle(String title)
		{
			this.label.setText(title);
		}

		private void initField()
		{
			EmptyBorder border = new EmptyBorder(0,0,0,2);
			this.label.setBorder(border);
			this.label.setOpaque(false);
		}

		private void initListener()
		{
			CloseCrossListener listener = new CloseCrossListener();
			this.button.addActionListener(listener);
		}

		private void initPane()
		{
			super.setOpaque(false);
			super.add(this.label,BorderLayout.CENTER);
			super.add(this.button,BorderLayout.EAST);
		}

		class CloseCrossListener implements ActionListener
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				AbstractElementTab tab = EditorTabbedPane.this.getSelectedElementTab();
				tab.processOnClose();

				TabCloseEvent event = new TabCloseEvent(tab,0);
				for(TabCloseListener listener : EditorTabbedPane.this.vectorTabCloseListener) {
					listener.tabClosed(event);
				}

				EditorTabbedPane.super.remove(tab);
			}
		}

		final class CloseCross extends JButton
		{
			private static final long serialVersionUID = -527164931538547134L;

			public CloseCross()
			{
				super();
				Dimension d = new Dimension(12,12);
				this.setMinimumSize(d);
				this.setPreferredSize(d);
				this.setMaximumSize(d);
			}

			@Override
			public void paint(Graphics g)
			{
				g.drawLine(3,3,11,11);
				g.drawLine(11,3,3,11);
			}
		}
	}
}
