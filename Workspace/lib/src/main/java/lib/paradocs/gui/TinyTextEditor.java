package lib.paradocs.gui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import lib.paradocs.io.ConfigParser;

public class TinyTextEditor extends JDialog
{
	private static final long serialVersionUID = -5210458053662546780L;

	private final JTextArea area = new JTextArea();
	private final JScrollPane scroll = new JScrollPane();
	private final JButton buttonDone = new JButton("Done");

	protected String textOriginal = "";
	protected String textModified = null;

	public TinyTextEditor(String title,boolean m,String original)
	{
		super();
		super.setTitle(title);
		super.setModal(m);

		this.textOriginal = original;

		this.initField();
		this.initListener();
		this.initPane();

		super.setResizable(true);
	}

	public TinyTextEditor(JFrame owner,String title,boolean m,String original)
	{
		super(owner,title,m);

		this.textOriginal = original;

		this.initField();
		this.initListener();
		this.initPane();

		super.setResizable(true);
	}

	public TinyTextEditor(JDialog owner,String title,boolean m,String original)
	{
		super(owner,title,m);

		this.textOriginal = original;

		this.initField();
		this.initListener();
		this.initPane();

		super.setResizable(true);
	}

	private void initField()
	{
		this.area.setLineWrap(true);
		this.area.setText(this.textOriginal);
		this.scroll.getViewport().setView(this.area);
		this.scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
	}

	private void initListener()
	{
		Listener listener = new Listener();
		this.buttonDone.addActionListener(listener);
		super.addWindowListener(listener);
	}

	private void initPane()
	{
		super.setLayout(new BorderLayout());

		JPanel paneSouth = new JPanel();
		paneSouth.add(this.buttonDone);

		super.add(this.scroll,BorderLayout.CENTER);
		super.add(paneSouth,BorderLayout.SOUTH);

		int width = ConfigParser.getValue("default_texteditor_width");
		int height = ConfigParser.getValue("default_texteditor_height");
		super.setSize(width,height);
	}

	public String getOriginalText()
	{
		return this.textOriginal;
	}

	public String getModifiedText()
	{
		return this.textModified;
	}

	class Listener extends WindowAdapter implements ActionListener
	{
		@Override
		public void windowClosing(WindowEvent e)
		{
			TinyTextEditor.this.textModified = TinyTextEditor.this.textOriginal;
			TinyTextEditor.super.dispose();
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			TinyTextEditor.this.textModified = TinyTextEditor.this.area.getText();
			TinyTextEditor.super.dispose();
		}
	}
}
