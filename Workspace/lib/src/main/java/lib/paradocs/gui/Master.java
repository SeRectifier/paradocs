package lib.paradocs.gui;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import javax.swing.*;
import javax.swing.filechooser.*;
import javax.swing.tree.*;
import lib.paradocs.core.*;
import lib.paradocs.gui.event.*;
import lib.paradocs.gui.tabs.FigureTab;
import lib.paradocs.gui.tabs.ParagraphTab;
import lib.paradocs.io.*;

public final class Master extends JFrame
{
	private static final long serialVersionUID = -9011895803031213192L;

	private Document document = new Document();
	private File fileCurrent = null;

	private final JMenuBar menubar = new JMenuBar();
	private final ArrayList<JMenuItem> listFileMenuItem = new ArrayList<JMenuItem>(0);
	private final ArrayList<JMenuItem> listEditMenuItem = new ArrayList<JMenuItem>(0);
	private final ArrayList<JMenuItem> listSettingsMenuItem = new ArrayList<JMenuItem>(0);

	private final DocumentTree tree = new DocumentTree(this.document);
	private final JScrollPane scrollTreeWrapper = new JScrollPane();
	private final EditorTabbedPane tabbedpane = new EditorTabbedPane();
	private final JButton buttonAdd = new JButton("Add");
	private final JButton buttonDelete = new JButton("Delete");
	private final JButton buttonRise = new JButton("Rise");
	private final JButton buttonFall = new JButton("Fall");
	private final JButton buttonMove = new JButton("Move");
	private final JFileChooser chooser = new JFileChooser();

	public Master()
	{
		super("Paradocs ALPHA");

		this.testcodes();;
		this.initField();
		this.initListener();
		this.initPane();

		super.setResizable(true);
	}

	private void testcodes()
	{
		this.document.addElement(new Paragraph("Paragraph"));
		this.document.addElement(new Figure("Figure"));
		Section section = new Section("Sample section");
		section.addChild(new Paragraph("Sample paragraph"));
		section.addChild(new Figure("Sample figure"));
		this.document.addElement(section);
		this.document.reflectChanges();
	}

	private void initField()
	{
		this.scrollTreeWrapper.getViewport().setView(this.tree);
		this.setJMenuBar(this.menubar);

		int widthChooser = ConfigParser.getValue("default_filechooser_width");
		int heightChooser = ConfigParser.getValue("default_filechooser_height");
		this.chooser.setPreferredSize(new Dimension(widthChooser,heightChooser));
		this.chooser.setFileFilter(new FileNameExtensionFilter("Paradocs original format (.paradoc)","paradoc"));
		this.chooser.setAcceptAllFileFilterUsed(false);

		JMenu[] arrayMenu = new JMenu[] {
				new JMenu("File"),
				new JMenu("Edit"),
				new JMenu("Settings"),
		};

		this.listFileMenuItem.add(new JMenuItem("New"));
		this.listFileMenuItem.add(new JMenuItem("Open..."));
		this.listFileMenuItem.add(new JMenuItem("Save"));
		this.listFileMenuItem.add(new JMenuItem("Save as..."));
		this.listFileMenuItem.add(new JMenuItem("Export..."));
		this.listFileMenuItem.add(new JMenuItem("Quit"));
		this.listEditMenuItem.add(new JMenuItem("Add section"));
		this.listEditMenuItem.add(new JMenuItem("Add paragraph"));
		this.listEditMenuItem.add(new JMenuItem("Add figure"));
		this.listEditMenuItem.add(new JMenuItem("Add table"));
		this.listEditMenuItem.add(new JMenuItem("Add image of table"));
		this.listEditMenuItem.add(new JMenuItem("Add list"));
		this.listEditMenuItem.add(new JMenuItem("Add chart"));
		this.listEditMenuItem.add(new JMenuItem("Add formula"));
		this.listEditMenuItem.add(new JMenuItem("Modify selected element"));
		this.listEditMenuItem.add(new JMenuItem("Delete selected element"));
		this.listSettingsMenuItem.add(new JMenuItem("Config..."));
		this.listSettingsMenuItem.add(new JMenuItem("About"));

		for(JMenuItem item : this.listFileMenuItem) arrayMenu[0].add(item);
		for(JMenuItem item : this.listEditMenuItem) arrayMenu[1].add(item);
		for(JMenuItem item : this.listSettingsMenuItem) arrayMenu[2].add(item);

		for(int i=0; i<arrayMenu.length; i++) this.menubar.add(arrayMenu[i]);
	}

	private void initListener()
	{
		EditorReflectionListener listenerEditorReflection = new EditorReflectionListener();
		MenuListener listenerMenu = new MenuListener();
		TreeListener listenerTree = new TreeListener();
		HandyListener listenerHandy = new HandyListener();
		QuitListener listenerQuit = new QuitListener();

		for(JMenuItem item : this.listFileMenuItem) item.addActionListener(listenerMenu);
		for(JMenuItem item : this.listEditMenuItem) item.addActionListener(listenerMenu);
		for(JMenuItem item : this.listSettingsMenuItem) item.addActionListener(listenerMenu);

		this.tabbedpane.addTabCloseListener(listenerEditorReflection);
		this.tree.addMouseListener(listenerTree);
		this.buttonAdd.addActionListener(listenerHandy);
		this.buttonDelete.addActionListener(listenerHandy);
		this.buttonRise.addActionListener(listenerHandy);
		this.buttonFall.addActionListener(listenerHandy);
		this.buttonMove.addActionListener(listenerHandy);
		super.addWindowListener(listenerQuit);
	}

	private void initPane()
	{
		JPanel paneHandy = new JPanel(new GridLayout(1,4));
		paneHandy.add(this.buttonAdd);
		paneHandy.add(this.buttonDelete);
		paneHandy.add(this.buttonRise);
		paneHandy.add(this.buttonFall);
		paneHandy.add(this.buttonMove);

		JPanel paneWest = new JPanel(new BorderLayout());
		paneWest.add(paneHandy,BorderLayout.SOUTH);
		paneWest.add(this.scrollTreeWrapper,BorderLayout.CENTER);

		super.add(paneWest,BorderLayout.WEST);
		super.add(this.tabbedpane,BorderLayout.CENTER);

		int width = ConfigParser.getValue("default_window_width");
		int height = ConfigParser.getValue("default_window_height");
		super.setSize(width,height);
	}

	protected void removeSelectedDocumentElement()
	{
		Element element = this.tree.getSelectedElement();
		if(element == null) return;
		if(this.document.matchesRootSection(element)) return;

		String title = "Delete";
		String message = "Delete \"" + element.getName() + "\"?";
		int res = JOptionPane.showConfirmDialog(this,message,title,JOptionPane.OK_CANCEL_OPTION);
		if(res == JOptionPane.OK_OPTION) {
			this.tabbedpane.closeElementTab(element);
			this.document.removeElement(element);
			this.document.reflectChanges();
		}
	}

	class MenuListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			Object src = e.getSource();

			String s = null;
			if(src instanceof JMenuItem) {
				JMenuItem mi = (JMenuItem) src;
				s = mi.getText();
			}
			else return;

			switch(s) {
			case "New":
				String message = "Close current document and create new one?";
				String title = "Create new";
				int res = JOptionPane.showConfirmDialog(Master.this.getContentPane(),message,title,JOptionPane.YES_NO_OPTION);
				if(res == JOptionPane.YES_OPTION) {
					Master.this.tabbedpane.closeAllElementTabs();
					Master.this.document = new Document();
					Master.this.tree.setDocument(Master.this.document);
				}
				break;
			case "Open...":
				Master.this.chooser.showOpenDialog(Master.this.getContentPane());
				File fileToLoad = Master.this.chooser.getSelectedFile();
				if(fileToLoad == null) return;
				try {
					Master.this.document = DocumentIO.read(fileToLoad);
					Master.this.tree.setDocument(Master.this.document);
					Master.this.fileCurrent = fileToLoad;
				}
				catch (ClassNotFoundException cnfe) {
					String messageWarning = "Selected file is not supported in this version";
					String titleWarning = "Load";
					JOptionPane.showMessageDialog(Master.this,messageWarning,titleWarning,JOptionPane.OK_OPTION);
					cnfe.printStackTrace();
				}
				catch (IOException ioe) {
					String messageWaring = "Failed to load file.";
					String titleWarning = "Load";
					JOptionPane.showMessageDialog(Master.this,messageWaring,titleWarning,JOptionPane.OK_OPTION);
					ioe.printStackTrace();
				}
				break;
			case "Save":
				if(Master.this.fileCurrent == null) {
					Master.this.chooser.showSaveDialog(Master.this.getContentPane());
					File fileToSave = Master.this.chooser.getSelectedFile();
					if(fileToSave == null) return;
					if(fileToSave.getName().endsWith(".paradoc") == false) 
						fileToSave = new File(fileToSave.getParent(),fileToSave.getName() + ".paradoc");
					try {
						DocumentIO.write(fileToSave,Master.this.document);
						Master.this.fileCurrent = fileToSave;
					}
					catch (IOException ioe) {
						String messageWaring = "Failed to save file";
						String titleWarning = "Save";
						JOptionPane.showMessageDialog(Master.this,messageWaring,titleWarning,JOptionPane.OK_OPTION);
						ioe.printStackTrace();
					}
				}
				else {
					try {
						DocumentIO.write(Master.this.fileCurrent,Master.this.document);
					}
					catch (IOException ioe) {
						String messageWaring = "Failed to save file";
						String titleWarning = "Save";
						JOptionPane.showMessageDialog(Master.this,messageWaring,titleWarning,JOptionPane.OK_OPTION);
						ioe.printStackTrace();
					}
				}
				break;
			case "Save as...":
				Master.this.chooser.showSaveDialog(Master.this.getContentPane());
				File fileToSave = Master.this.chooser.getSelectedFile();
				if(fileToSave == null) return;
				if(fileToSave.getName().endsWith(".paradoc") == false) 
					fileToSave = new File(fileToSave.getParent(),fileToSave.getName() + ".paradoc");
				try {
					DocumentIO.write(fileToSave,Master.this.document);
					Master.this.fileCurrent = fileToSave;
				}
				catch (IOException ioe) {
					String messageWaring = "Failed to save file";
					String titleWarning = "Save";
					JOptionPane.showMessageDialog(Master.this,messageWaring,titleWarning,JOptionPane.OK_OPTION);
					ioe.printStackTrace();
				}
				break;
			case "Export...":
				
				break;
			case "Quit":
				System.exit(0);
				break;
			case "Add section":
				
				break;
			case "Add paragraph":
				
				break;
			case "Add figure":
				
				break;
			case "Add table":
				
				break;
			case "Add image of table":
				
				break;
			case "Add list":
				
				break;
			case "Add chart":
				
				break;
			case "Add formula":
				
				break;
			case "Modify selected element":
				
				break;
			case "Delete selected element":
				Master.this.removeSelectedDocumentElement();
				break;
			case "Config...":
				
				break;
			case "About":
				
				break;
			default:
				break;
			}
		}
	}

	class TreeListener extends MouseAdapter
	{
		@Override
		public void mousePressed(MouseEvent e)
		{
			TreePath path = Master.this.tree.getPathForLocation(e.getX(),e.getY());
			if(e.getClickCount() != 2) return;
			if(path == null) return;

			Object obj = path.getLastPathComponent();

			if(obj instanceof Paragraph) {
				Paragraph p = (Paragraph) obj;
				ParagraphTab tab = new ParagraphTab(p);
				tab.addElementUpdateListener(new EditorReflectionListener());
				Master.this.tabbedpane.openElementTab(tab);
			}
			if(obj instanceof Figure) {
				Figure f = (Figure) obj;
				FigureTab tab = new FigureTab(f);
				tab.addElementUpdateListener(new EditorReflectionListener());
				Master.this.tabbedpane.openElementTab(tab);
			}
		}
	}

	class EditorReflectionListener implements TabCloseListener,ElementUpdateListener
	{
		@Override
		public void tabClosed(TabCloseEvent e)
		{
			Master.this.document.reflectTerminalModification(e.getTab().getTarget());
		}

		@Override
		public void elementUpdated(ElementUpdateEvent e)
		{
			Master.this.document.reflectTerminalModification(e.getElement());
		}
	}

	class HandyListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			Object src = e.getSource();

			if(src.equals(Master.this.buttonAdd)) {
				NewElementDialog dialog = new NewElementDialog(Master.this);
				dialog.setVisible(true);
			}

			if(src.equals(Master.this.buttonDelete)) 
				Master.this.removeSelectedDocumentElement();
		}
	}

	class QuitListener extends WindowAdapter implements ActionListener
	{
		@Override
		public void windowClosing(WindowEvent e)
		{
			System.exit(0);
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			System.exit(0);
		}
	}
}
